(function ($) {
  Drupal.viewsSlideshowXtra = Drupal.viewsSlideshowXtra || {};

  Drupal.viewsSlideshowXtra.transitionBegin = function (options) {
    // Find our views slideshow xtra element
    $('[id^="views-slideshow-xtra-"]:not(.views-slideshow-xtra-processed)').addClass('views-slideshow-xtra-processed').each(function() {

      // Get the slide box.
      var slideArea = $(this).parent().parent().find('.views_slideshow_main');

      // Remove the views slideshow xtra html from the dom
      var xtraHTML = $(this).detach();

      // Attach the views slideshow xtra html to below the main slide.
      $(xtraHTML).appendTo(slideArea);

      // Get the offsets of the slide and the views slideshow xtra elements
      var slideAreaOffset = slideArea.offset();
      var xtraOffset = $(this).offset();

      // Move the views slideshow xtra element over the top of the slide.
      var marginMove = slideAreaOffset.top - xtraOffset.top;
      $(this).css({'margin-top': marginMove + 'px'});

      // Move our text slide elements into place.
      var slideData = Drupal.settings.viewsSlideshowXtra[options.slideshowID].slideInfo.text;
      var slideNum = 0;
      for (slide in slideData) {
        var items = slideData[slide];
        var itemNum = 0;
        for (item in items) {
          $('#views-slideshow-xtra-' + options.slideshowID + ' .views-slideshow-xtra-text-' + slideNum + '-' + itemNum).css({
            'margin-top': items[item].top,
            'margin-left': items[item].left,
            'width': slideArea.width()
          });
          itemNum++;
        }
        slideNum++;
      }

      // Move our link slide elements into place.
      var slideData = Drupal.settings.viewsSlideshowXtra[options.slideshowID].slideInfo.link;
      var slideNum = 0;
      for (slide in slideData) {
        var items = slideData[slide];
        var itemNum = 0;
        for (item in items) {
          $('#views-slideshow-xtra-' + options.slideshowID + ' .views-slideshow-xtra-link-' + slideNum + '-' + itemNum).css({
            'margin-top': items[item].top,
            'margin-left': items[item].left,
            'width': slideArea.width()
          });
          itemNum++;
        }
        slideNum++;
      }
    });

    var settings = Drupal.settings.viewsSlideshowXtra[options.slideshowID];

    // Hide elements either by fading or not
    if (settings.displayDelayFade) {
      $('#views-slideshow-xtra-' + options.slideshowID + ' .views-slideshow-xtra-row').fadeOut();
    }
    else {
      $('#views-slideshow-xtra-' + options.slideshowID + ' .views-slideshow-xtra-row').hide();
    }

    // Pause from showing the text and links however long the user decides.
    setTimeout(function() {
      if (settings.displayDelayFade) {
        $('#views-slideshow-xtra-' + options.slideshowID + ' .views-slideshow-xtra-row-' + options.slideNum).fadeIn();
      }
      else {
        $('#views-slideshow-xtra-' + options.slideshowID + ' .views-slideshow-xtra-row-' + options.slideNum).show();
      }
    },
    settings.displayDelay
    );
  }
})(jQuery);
