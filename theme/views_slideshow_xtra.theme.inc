<?php
/**
 * Views Slideshow Xtra theme.
 *
 * @ingroup themeable
 */
function theme_views_slideshow_xtra_widget_render($vars) {
  // Add javascript settings for the pager type.
  $js_vars = array(
    'viewsSlideshowXtra' => array(
      $vars['vss_id'] => array(
        'displayDelay' => $vars['settings']['display_delay'],
        'displayDelayFade' => $vars['settings']['display_delay_fade'],
      ),
    ),
  );

  drupal_add_js($js_vars, 'setting');

  $output = '<div id="views-slideshow-xtra-' . $vars['vss_id'] . '" class="views-slideshow-xtra-wrapper">';


  $field_types = array('text_field', 'link_field', 'lightbox_link_field');
  // Render all the fields.
  foreach ($vars['view']->result as $count => $node) {
    $rendered_fields = '';
    $output .= '<div class="views-slideshow-xtra-row views-slideshow-xtra-row-' . $count . '">';
    foreach ($vars['settings']['fields'] as $field) {
      if ($field) {
        if (is_object($vars['view']->field[$field])) {
          $field_values_stub = $vars['view']->result[$count]->_field_data['nid']['entity']->$field;
          $field_item_num = 0;
          foreach ($field_values_stub['und'] as $field_info) {
            $field_data = drupal_json_decode($field_info['value']);
            if (is_null($field_data)) {
              $field_data = array();
            }

            $field_data += array(
              'type' => 'text',
              'left' => 0,
              'top' => 0,
              'text' => '',
              'url' => '',
              'modal' => false,
              'width' => 900,
              'height' => 600,
            );

            $output .= theme('views_slideshow_xtra_' . $field_data['type'], array('vss_id' => $vars['vss_id'], 'view' => $vars['view'], 'field' => $field_data, 'slide_count' => $count, 'field_item_count' => $field_item_num));
            $field_item_num++;
          }
        }
      }
    }
    $output .= '</div>';
  }

  $output .= '</div>';

  return $output;
}

function template_preprocess_views_slideshow_xtra_text(&$vars) {
  $js_vars = array(
    'viewsSlideshowXtra' => array(
      $vars['vss_id'] => array(
        'slideInfo' => array(
          'text' => array(
            'slide-' . $vars['slide_count'] => array(
              'item-' . $vars['field_item_count'] => array(
                'left' => $vars['field']['left'],
                'top' => $vars['field']['top'],
              ),
            ),
          ),
        ),
      ),
    ),
  );

  drupal_add_js($js_vars, 'setting');
}

/**
 * Views Slideshow Xtra Link theme
 */
function template_preprocess_views_slideshow_xtra_link(&$vars) {
  $js_vars = array(
    'viewsSlideshowXtra' => array(
      $vars['vss_id'] => array(
        'slideInfo' => array(
          'link' => array(
            'slide-' . $vars['slide_count'] => array(
              'item-' . $vars['field_item_count'] => array(
                'left' => $vars['field']['left'],
                'top' => $vars['field']['top'],
              ),
            ),
          ),
        ),
      ),
    ),
  );

  drupal_add_js($js_vars, 'setting');

  if ($vars['field']['modal']) {
    $vars['field']['url'] .= '?width=' . $vars['field']['width'] . '&height=' . $vars['field']['height'] . '&iframe=true';
  }
}